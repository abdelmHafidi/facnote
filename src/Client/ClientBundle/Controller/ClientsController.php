<?php

namespace Client\ClientBundle\Controller;

use Client\ClientBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class ClientsController extends Controller
{
    // public function indexAction()
    // {
    //     return $this->render('ClientBundle:Default:index.html.twig');
    // }


    
    /**
     * Afficher tout les clients
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //la requete pour recuperer les clients
        $clients = $em->getRepository("ClientBundle:Client")->getListeClient();

        $total=$em->getRepository("ClientBundle:Client")->getTotalClient();

        return $this->render('@Client/Clients/index.html.twig', array(
            'clients' => $clients,
            'total'=>$total
        ));
    }

    /**
     * Afficher les informations du client "id"
     */
    public function showAction(Client $client)
    {
        return $this->render('@Client/Clients/show.html.twig', array(
            'client' => $client,
        ));
    }

    /**
     * Afficher le formulaire pour l'ajout et  la modification de client
     */
    public function addEditAction(Request $request, $id=null)
    {
        $em=$this->getDoctrine()->getManager();
        $_editMode=0;

        if(isset($id)){
            $client=$em->getRepository('ClientBundle:Client')->find($id);
            if(!$client){
                throw $this->createNotFoundException('client non trouvé');
            }
            $_editMode=1;
        } else {
            $client=new Client();
        }

        $form = $this->createForm('Client\ClientBundle\Form\ClientType', $client);
        

        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($client);
                $em->flush();
    
                return $this->redirectToRoute('_show', array('id' => $client->getId()));
            }
        }

        return $this->render('@Client/Clients/formClient.html.twig', array(
            'client' => $client,
            'editMode'=>$_editMode,
            'formClient' => $form->createView(),
        ));
    }

    /**
     * supprimer un client
     */
    public function deleteClientAction($idClient){

        $em=$this->getDoctrine()->getManager();

        $clientRepository=$em->getRepository("ClientBundle:Client");

        $client=$clientRepository->find($idClient);

        if(is_null($client)){
            throw $this->createNotFoundException("bof");
        }

        $em->remove($client);
        $em->flush();

        return $this->redirectToRoute('_index');
    }

    /**
     * La liste des clients en PDF
     * 
     */
    public function clientToPdfAction(){

        $pdf=$this->get('knp_snappy.pdf');

        $em=$this->getDoctrine()->getManager();

        $clientRepository=$em->getRepository("ClientBundle:Client");

        $client=$clientRepository->findAll();

        $html=$this->renderView("@Client/Clients/liste.html.twig",[
            "clients"=>$client
        ]);

        $nom_fichier="liste-des-clients";

        return new Response(
            $pdf->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => "application/pdf",
                'Content-Disposition'=>'attachment, filename="'.$nom_fichier.'.pdf"'
            )
        );
    }
}
